#![no_std]	// no standard library
#![no_main] // no main-function

extern crate cortex_m;
extern crate panic_halt;
extern crate tm4c123x_hal as hal;

use cortex_m::asm;
use cortex_m_rt::entry;
use hal::delay::Delay;
use hal::prelude::*;

// struct for storing the states of the LEDs because the current
// implementation has no functionality for reading output-pins
#[derive(Default)] // macro for deriving a default-implementation
struct LedState {
	red: bool,
	blue: bool,
	green: bool,
}

#[entry]
fn main() -> ! {
	let ti = hal::Peripherals::take().unwrap(); // take TIVA peripherals
	let cpu = cortex_m::Peripherals::take().unwrap(); // take CPU peripherals

	let mut sc = ti.SYSCTL.constrain();

	// setup the clock to 80MHz PLL clocked with the main-oscillator
	//
	// NOTE: with this clock-config it's not possible to debug this application
	// due to some weird error either in the debug-chip from this evalboard or
	// in openOCD. However, if another clock-source is used the 
	// baudrate-calculation is for some reason not longer correct which results
	// in incorrect transmitted and received bytes
	sc.clock_setup.oscillator = hal::sysctl::Oscillator::Main(
		hal::sysctl::CrystalFrequency::_16mhz,
		hal::sysctl::SystemClock::UsePll(hal::sysctl::PllOutputFrequency::_80_00mhz),
	);
	let clocks = sc.clock_setup.freeze();
	let mut delay = Delay::new(cpu.SYST, &clocks);

	// setup the dedicated pins of the RGB-LED to output-pins
	let portf = ti.GPIO_PORTF.split(&sc.power_control);
	let mut led_red = portf.pf1.into_push_pull_output();
	let mut led_blue = portf.pf2.into_push_pull_output();
	let mut led_green = portf.pf3.into_push_pull_output();

	// setup the GPIO-pins for UART0 correctly
	let mut porta = ti.GPIO_PORTA.split(&sc.power_control);
	let tx = porta.pa1.into_af_push_pull::<hal::gpio::AF1>(&mut porta.control);
	let rx = porta.pa0.into_af_push_pull::<hal::gpio::AF1>(&mut porta.control);

	// create an instance of UART0
	let uart0 = hal::serial::Serial::uart0(
		ti.UART0,
		tx, // TX pin
		rx, // RX pin
		(), // rts not used
		(), // cts not used
		115200.bps(), // 115200 bits/s
		hal::serial::NewlineMode::Binary,
		&clocks,
		&sc.power_control,
	);

	// get back tx- and rx-pin from UART0 in order to read and write to/from them
	let (mut tx, mut rx) = uart0.split();

	let mut led_state = LedState::default(); // store actual LED states
	let mut b: [u8; 1] = [0u8; 1]; // buffer for transmitting data over UART

	loop {
		// check if data was received
		match rx.read() {
			Ok(val) => {
				// a character was received successfully
				b[0] = val; // store received byte in transmit-buffer
				tx.write_all(&b); // echo received character back

				if (b[0] >= b'a') && (b[0] <= b'z') {
					// if a character between 'a' and 'z' was received, toggle
					// the red LED

					led_state.red = !led_state.red;

					if led_state.red {
						led_red.set_high();
					} else {
						led_red.set_low();
					}
				} else if (b[0] >= b'A') && (b[0] <= b'Z') {
					// if a character between 'A' and 'Z' was received, toggle
					// the green LED

					led_state.green = !led_state.green;

					if led_state.green {
						led_green.set_high();
					} else {
						led_green.set_low();
					}
				} else {
					// if any other character than 'a' - 'z' or 'A' - 'Z' was
					// received, toggle the blue LED

					led_state.blue = !led_state.blue;

					if led_state.blue {
						led_blue.set_high();
					} else {
						led_blue.set_low();
					}
				}
			}
			_ => {} // not sure if this this is an error or no byte was received
		};
	}
}
