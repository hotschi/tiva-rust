# UART-Example

In this example a UART-echo was implemented. It just returns every character
exactly as it was received. Additionally it toggles the RGB-LED depending on the
received character:
 + a-z: toggle the red LED
 + A-Z: toggle the green LED
 + all remaining characters: toggle the blue LED