#![no_std] // macro for indicating that no standard-library is used -> bare-metal
#![no_main] // macro for indicating that no main-function exists (-> #[entry] macro)

// provides an API to support all functions of the Cortex-M based CPUs
extern crate cortex_m;
// provides a set of functions to handle panics (e.g. the panic!() macro)
extern crate panic_halt;
// provides an API to support all peripheral function of the TM4C123GXL uC
extern crate tm4c123x_hal as hal;

use cortex_m_rt::entry;
use hal::delay::Delay;
use hal::prelude::*;
use hal::tm4c123x;

#[entry] // define the following function as entry-function
// the '!' return value indicates that this function will never return
fn main() -> ! {
	// take the ownership of the microcontroller peripheral
	let p = hal::Peripherals::take().unwrap();
	
	// take the ownership of the Cortex M4F peripherals
	let c = cortex_m::Peripherals::take().unwrap();
	
	// If PLL is used with the main-clock, openocd creates errors for some reason..
	/*
	sc.clock_setup.oscillator = hal::sysctl::Oscillator::Main(
		hal::sysctl::CrystalFrequency::_16mhz,
		hal::sysctl::SystemClock::UsePll(hal::sysctl::PllOutputFrequency::_80_00mhz),
	);
	*/
	
	// create a Sysctl struct which provides a higher-level API
	let mut sc = p.SYSCTL.constrain();
	
	// setup the system clock to the internal precision oscillator with 16MHz
	sc.clock_setup.oscillator = hal::sysctl::Oscillator::PrecisionInternal(
		hal::sysctl::SystemClock::UseOscillator(hal::sysctl::Divider::_1),
	);
	
	// freeze the clock-config so that they cant be changed by software anymore
	let clocks = sc.clock_setup.freeze();
	
	// setup a delay objects which is based on the systick-counter
	let mut delay = Delay::new(c.SYST, &clocks);
	
	// get an objects which represents the PORTF-peripheral of the uC
	let portf = p.GPIO_PORTF.split(&sc.power_control);
	
	// setup the pin PF1 into an output due to the connected blue LED
	let mut led_blue = portf.pf1.into_push_pull_output();
		
	let mut cnt = 0u64; // a simple counter
	loop {
		cnt += 1; // increment counter
		
		// toggle LED every 50ms
		delay.delay_ms(50u16);
		led_blue.set_high();
		delay.delay_ms(50u16);
		led_blue.set_low();
	}
}