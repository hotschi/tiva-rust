# I2C Example

This project is an example of how to use the I2C-interface of the TIVA in a
blocking way. A DS1621-chip was used as an I2C-slave. It's a temperature-
sensor which can deliver temperature-values over I2C.

This example configures this sensor in a way that it continuously converts
temperature-values and that one can read them out continuously too. After the
raw-data was read out, it gets converted into a float-value.

The datasheet for the chip can be found in doc/datasheets.