#![no_std] // no standard-library
#![no_main] // no main-function

extern crate cortex_m;
extern crate panic_halt;
extern crate tm4c123x_hal as hal;

use cortex_m::asm;
use cortex_m_rt::entry;
use hal::delay::Delay;
use hal::prelude::*;
use hal::tm4c123x;

// I2C address of the DS1621
const DS_ADDR: u8 = 0x48;
// config for contiuous conv.     |reg|  |val|
const DS_SET_CONTINUOUS: &[u8] = &[0xAC, 0x02];
// config for starting the conversion
const DS_START_CONVERSION: &[u8] = &[0xEE];

#[entry]
fn main() -> ! {
	let ti = hal::Peripherals::take().unwrap(); // take TIVA peripherals
	let cpu = cortex_m::Peripherals::take().unwrap(); // take CPU peripherals
	
	// If PLL is used with the main-clock, openocd creates errors for some reason..
	/*
	sc.clock_setup.oscillator = hal::sysctl::Oscillator::Main(
		hal::sysctl::CrystalFrequency::_16mhz,
		hal::sysctl::SystemClock::UsePll(hal::sysctl::PllOutputFrequency::_80_00mhz),
	);
	*/

	// setup clock to 16MHz Internal
	let mut sc = ti.SYSCTL.constrain();
	sc.clock_setup.oscillator = hal::sysctl::Oscillator::PrecisionInternal(
		hal::sysctl::SystemClock::UseOscillator(hal::sysctl::Divider::_1),
	);
	
	// freeze the clocks so that they cant be changed by software anymore
	let clocks = sc.clock_setup.freeze();
	let mut delay = Delay::new(cpu.SYST, &clocks); // setup a delay-object
	
	// port A contains the pins for the I2C1 module
	let mut porta = ti.GPIO_PORTA.split(&sc.power_control);
	let sda = porta.pa7.into_af_open_drain(&mut porta.control);
	let scl = porta.pa6.into_af_push_pull(&mut porta.control);
	
	// setup the I2C1 module
	let mut i2c = hal::i2c::I2c::i2c1(
		ti.I2C1,    // 1st i2c module
		(scl, sda), // pin-config
		400.khz(),  // 400kHz
		&clocks,
		&sc.power_control,
	);
	
	let mut temp_data: [u8; 2] = [0x00, 2]; // byte-array to store raw-values
	let mut temperature: f32 = 0.0; // float value to store actual temperature
	
	// config DS1621 before reading the temperature
	i2c.write(DS_ADDR, DS_SET_CONTINUOUS).unwrap(); // set to contiuous mode
	i2c.write(DS_ADDR, DS_START_CONVERSION).unwrap(); // start conversion
	
	loop {
		// command to read temperature
		i2c.write(DS_ADDR, &[0xAA]).unwrap();
		// read back 2 bytes which contain the temperature
		i2c.read(DS_ADDR, &mut temp_data).unwrap();
		
		// convert the received bytes to an actual temperature
		temperature = temp_data[0] as f32;
		temperature += if temp_data[1] > 0 {
			0.5
		} else {
			0.0
		};
	
		delay.delay_ms(200u16);
	}
}