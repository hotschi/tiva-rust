#![no_std]
#![no_main]

// pick a panicking behavior
extern crate panic_halt;
extern crate cortex_m;
extern crate tm4c123x_hal as hal;

use cortex_m::asm;
use cortex_m_rt::entry;
use hal::prelude::*;
use hal::delay::Delay;
use hal::tm4c123x;

#[entry]
fn main() -> ! {
    asm::nop(); // To not have main optimize to abort in release mode, remove when you add code

    loop {
        // your code goes here
    }
}
