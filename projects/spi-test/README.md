# SPI Example

This project is an example of how to use the SPI-interface of the TIVA in a
blocking way. An EEPROM with 512Bytes memory (25LC040 from Microchip) was used
as an SPI-slave. The datasheet for this EEPROM can be found in /doc/datasheets. 

This example implements the basic funcionality to read and write data from/to
the EEPROM. Also an erase() function is provided which overwrites the whole 
EEPROM with a certain value. 

The functions basically work, but sometimes the write-functions don't work 
properly because there are obviously some timing-problems. 