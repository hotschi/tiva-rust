#![no_std] 	// no standard library
#![no_main] // no main-function

extern crate cortex_m;
extern crate panic_halt;
extern crate tm4c123x_hal as hal;

use cortex_m::asm;
use cortex_m_rt::entry;
use hal::delay::Delay;
use hal::gpio::gpioa::*;
use hal::gpio::*;
use hal::prelude::*;
use hal::spi::*;
use hal::tm4c123x::*;

// struct for the EEPROM which contains the SPI module and a chipselect pin
struct EEPROM25XX040 {
	spi: Spi<
		SSI0,
		(
			PA2<AlternateFunction<AF2, PushPull>>,
			PA4<AlternateFunction<AF2, PushPull>>,
			PA5<AlternateFunction<AF2, PushPull>>,
		),
	>,
	cs: PA3<Output<PushPull>>,
}

// implement some member-functions for the EEPROM25XX040 struct in order to 
// make it easier to read/write from/to the EEPROM
impl EEPROM25XX040 {
	const IDX_A8: usize = 3;	// idx of the A8-bit within the frame
	const CMD_WREN: u8 = 0x06;	// cmd for enabling writing to the EEPROM
	const CMD_WRDI: u8 = 0x04;	// cmd for disabling writing to the EEPROM
	const CMD_WRITE: u8 = 0x02;	// cmd for writing to the EEPROM
	const CMD_READ: u8 = 0x03;	// cmd for reading from the EEPROM
	
	fn write(&mut self, addr: u8, data: &[u8]) {
		// this function does not take care of page-boundaries!!!
		// if you write more than 16bytes or cross a page-boundary with less
		// than 16bytes (1 page = 16bytes, e.g. write 5bytes to address 0x2D
		// a page-boundary will occur and the data won't be written)
		
		// enable writing
		self.cs.set_low();
		self.spi.send(EEPROM25XX040::CMD_WREN).unwrap();
		self.cs.set_high();
		
		// send write command
		self.cs.set_low();
		self.spi.send(EEPROM25XX040::CMD_WRITE).unwrap();
		self.spi.send(addr).unwrap();
		
		// write data into the EEPROM
		for i in 0..data.len() {
			self.spi.send(data[i]).unwrap();
		}
		self.cs.set_high();
	}
	
	fn read(&mut self, addr: u8, data: &mut [u8]) {
		// setup reading from the EEPROM
		self.cs.set_low();
		self.spi.send(EEPROM25XX040::CMD_READ).unwrap();
		self.spi.send(addr).unwrap();
		
		// read the data from the EEPROM into the provided buffer
		for i in 0..data.len() {
			// send dummy-data in order to receive something
			self.spi.send(0x00).unwrap();
			let val = self.spi.read().unwrap();
			data[i] = val;
		}
		self.cs.set_high();
	}
	
	fn erase(&mut self, erase_val: u8, delay: &mut Delay) {
		// this function 'erases' the whole EEPROM with a certain value which can
		// be specified by calling the function ('erase_val')
		
		// iterate over all 32 pages (32 * 16bytes = 512bytes)
		for a in 0..32 {
			
			// set the A8 bit if the page is greater than 15
			let a8 = ((a > 15) as u8) << EEPROM25XX040::IDX_A8; // avoid branching
			let addr = a << 4;
			
			// setup writing
			self.cs.set_low();
			self.spi.send(EEPROM25XX040::CMD_WREN).unwrap();
			self.cs.set_high();
			
			// send write-command
			self.cs.set_low();
			self.spi.send(EEPROM25XX040::CMD_WRITE | a8).unwrap();
			self.spi.send(addr).unwrap();
			
			// erase the whole page with the erase_val
			for _ in 0..16 {
				self.spi.send(erase_val).unwrap();
			}
			
			self.cs.set_high();
			delay.delay_ms(6u32); // needs 5ms to write into eeprom
		}
	}
}

#[entry]
fn main() -> ! {
	let ti = hal::Peripherals::take().unwrap(); // take TIVA peripherals
	let cpu = cortex_m::Peripherals::take().unwrap(); // take CPU peripherals
	
	// If PLL is used with the main-clock, openocd creates errors for some reason..
	/*
	sc.clock_setup.oscillator = hal::sysctl::Oscillator::Main(
		hal::sysctl::CrystalFrequency::_16mhz,
		hal::sysctl::SystemClock::UsePll(hal::sysctl::PllOutputFrequency::_80_00mhz),
	);
	*/

	// setup clock to 16MHz internal oscillator
	let mut sc = ti.SYSCTL.constrain();	
	sc.clock_setup.oscillator = hal::sysctl::Oscillator::PrecisionInternal(
		hal::sysctl::SystemClock::UseOscillator(hal::sysctl::Divider::_1),
	);
	let clocks = sc.clock_setup.freeze();
	let mut delay = Delay::new(cpu.SYST, &clocks);
	
	// setup the GPIO-pins properly in order to serve as SPI-pins
	let mut porta = ti.GPIO_PORTA.split(&sc.power_control);
	let mosi = porta.pa5.into_af_push_pull::<hal::gpio::AF2>(&mut porta.control);
	let miso = porta.pa4.into_af_push_pull::<hal::gpio::AF2>(&mut porta.control);
	let clk = porta.pa2.into_af_push_pull::<hal::gpio::AF2>(&mut porta.control);
	let cs = porta.pa3.into_push_pull_output();
	
	// setup the SPI0-module
	let spi0 = hal::spi::Spi::spi0(
		ti.SSI0,
		(clk, miso, mosi),
		hal::spi::MODE_0,
		hal::time::KiloHertz(1000),
		&clocks,
		&sc.power_control,
	);
	
	// create an EEPROM struct
	let mut eepr = EEPROM25XX040 {
		spi: spi0,
		cs: cs,
	};
	
	let mut cnt: [u8; 1] = [0x00]; // counter
	let mut read_data = [0; 5]; // array for reading values back from the EEPROM
	const DUMMY_DATA: &[u8] = &[0xDE, 0xAD, 0xBE, 0xEF];
	
	eepr.erase(0x00, &mut delay); // erase EEPROM with 0
	eepr.write(0x10, DUMMY_DATA); // write DUMMY_DATA into EEPROM
	
	loop {
		eepr.write(0x14, &cnt); // write counter into EEPROM
		eepr.read(0x10, &mut read_data); // read back 5 bytes
		cnt[0] += 1; // increment counter
		delay.delay_ms(200u16);
	}
}