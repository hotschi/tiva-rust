#![no_std]
#![no_main]

use core::sync::atomic::*;

#[derive(Debug)]
pub enum FifoErr {
	Full,
	Empty,
	Undefined,
}

pub struct Fifo<'a> {
	buf: &'a mut [u8],
	idx_in: AtomicUsize,
	idx_out: AtomicUsize,
}

impl<'a> Fifo<'a> {
	pub fn new(buf: &'a mut [u8]) -> Self {
		Self {
			buf: buf,
			idx_in: AtomicUsize::new(0),
			idx_out: AtomicUsize::new(0),
		}
	}

	pub fn push(&mut self, data: u8) -> Result<(), FifoErr> {
		let idx_o = self.idx_out.load(Ordering::Relaxed);
		let mut idx_i = self.idx_in.load(Ordering::SeqCst);

		if ((idx_i + 1) % self.buf.len()) == idx_o {
			return Err(FifoErr::Full);
		}

		self.buf[idx_i] = data;
		idx_i += 1;

		if idx_i >= self.buf.len() {
			idx_i = 0;
		}

		self.idx_in.store(idx_i, Ordering::SeqCst);
		Ok(())
	}

	pub fn pop(&mut self) -> Result<u8, FifoErr> {
		let idx_i = self.idx_in.load(Ordering::Relaxed);
		let mut idx_o = self.idx_out.load(Ordering::SeqCst);

		if idx_i == idx_o {
			return Err(FifoErr::Empty);
		}

		let ret = self.buf[idx_o];
		idx_o += 1;

		if idx_o >= self.buf.len() {
			idx_o = 0;
		}

		self.idx_out.store(idx_o, Ordering::SeqCst);
		Ok(ret)
	}

	pub fn empty(&self) -> bool {
		if self.idx_in.load(Ordering::Relaxed) == self.idx_out.load(Ordering::Relaxed) {
			true
		} else {
			false
		}
	}

	pub fn full(&self) -> bool {
		if self.used() == self.buf.len() {
			true
		} else {
			false
		}
	}

	pub fn available(&self) -> usize {
		let idx_i = self.idx_in.load(Ordering::Relaxed);
		let idx_o = self.idx_out.load(Ordering::Relaxed);

		if idx_i >= idx_o {
			(self.buf.len() - (idx_i - idx_o))
		} else {
			(idx_o - idx_i)
		}
	}

	pub fn used(&self) -> usize {
		let idx_i = self.idx_in.load(Ordering::Relaxed);
		let idx_o = self.idx_out.load(Ordering::Relaxed);

		if idx_i >= idx_o {
			(idx_i - idx_o)
		} else {
			(self.buf.len() - (idx_o - idx_i))
		}
	}
}
