#![no_std]
#![no_main]

// pick a panicking behavior
extern crate cortex_m;
extern crate panic_halt;
extern crate tm4c123x_hal as hal;

use core::cell::{RefCell, UnsafeCell};
use core::ops::DerefMut;
use cortex_m::asm;
use cortex_m::interrupt::Mutex;
use cortex_m_rt::{entry, exception};
use hal::delay::Delay;
use hal::prelude::*;
use hal::tm4c123x::{interrupt, Interrupt, UART0};
use hal::{gpio, sysctl::*, time};

mod fifo;
use fifo::*;

struct AsyncUART0<'a> {
	uart: UART0,
	_tx: gpio::gpioa::PA1<gpio::AlternateFunction<gpio::AF1, gpio::PushPull>>,
	_rx: gpio::gpioa::PA0<gpio::AlternateFunction<gpio::AF1, gpio::PushPull>>,
	tx_fifo: Fifo<'a>,
	rx_fifo: Fifo<'a>,
}

impl<'a> AsyncUART0<'a> {
	fn new(
		uart: UART0,
		tx: gpio::gpioa::PA1<gpio::AlternateFunction<gpio::AF1, gpio::PushPull>>,
		rx: gpio::gpioa::PA0<gpio::AlternateFunction<gpio::AF1, gpio::PushPull>>,
		pc: &mut PowerControl,
		clocks: &Clocks,
		baudrate: time::Bps,
		tx_buf: &'a mut [u8],
		rx_buf: &'a mut [u8],
	) -> Self {
		control_power(pc, Domain::Uart0, RunMode::Run, PowerState::On);
		reset(pc, Domain::Uart0);
		uart.ctl.reset();
		// disable uart before configuration
		uart.ctl.write(|w| w.uarten().clear_bit());

		// set baud-rate
		let baud_int: u32 = (((clocks.sysclk.0 * 8) / baudrate.0) + 1) / 2;
		uart.ibrd.write(|w| unsafe { w.divint().bits((baud_int / 64) as u16) });
		uart.fbrd.write(|w| unsafe { w.divfrac().bits((baud_int % 64) as u8) });

		uart.lcrh.write(|w| w.wlen()._8()); // set word-length to 8bit

		uart.im.write(|w| w.txim().set_bit().rxim().set_bit()); // enable tx- and rx-interrupt

		// the CTL register must be configured in this weird way, otherwise
		// the config won't stay in the register for some reason
		uart.ctl.write(|w| w.eot().set_bit()); // enable transmit-interrupt when 1byte was sent
									   // enable uart, transmitter and receiver
		uart.ctl.write(|w| w.uarten().set_bit().txe().set_bit().rxe().set_bit());

		Self {
			uart: uart,
			_tx: tx,
			_rx: rx,
			tx_fifo: Fifo::new(tx_buf),
			rx_fifo: Fifo::new(rx_buf),
		}
	}

	fn received_bytes(&self) -> usize {
		self.rx_fifo.used()
	}

	fn write(&mut self, data: &[u8]) -> Result<(), FifoErr> {
		if self.tx_fifo.available() < data.len() {
			return Err(FifoErr::Full);
		}

		for b in data {
			self.tx_fifo.push(b.clone()).unwrap();
		}

		if !self.uart.fr.read().busy().bit() {
			let word = self.tx_fifo.pop().unwrap() as u32;
			self.uart.dr.write(move |w| unsafe { w.bits(word) });
		}

		Ok(())
	}

	fn read(&mut self, buf: &mut [u8]) -> usize {
		if self.rx_fifo.available() >= buf.len() {
			for i in 0..buf.len() {
				buf[i] = self.rx_fifo.pop().unwrap();
			}

			buf.len()
		} else {
			for i in 0..self.rx_fifo.available() {
				buf[i] = self.rx_fifo.pop().unwrap();
			}

			self.rx_fifo.available()
		}
	}
}

static ASYNC_UART: Mutex<RefCell<Option<AsyncUART0>>> = Mutex::new(RefCell::new(None));
static mut RX_BUF: [u8; 512] = [0; 512];
static mut TX_BUF: [u8; 512] = [0; 512];

#[entry]
fn main() -> ! {
	let mut ti = hal::Peripherals::take().unwrap();
	let mut ti_core = hal::CorePeripherals::take().unwrap();

	let mut sc = ti.SYSCTL.constrain();

	// If PLL is used with the main-clock, openocd creates errors for some reason..

	sc.clock_setup.oscillator = hal::sysctl::Oscillator::Main(
		hal::sysctl::CrystalFrequency::_16mhz,
		hal::sysctl::SystemClock::UsePll(hal::sysctl::PllOutputFrequency::_80_00mhz),
	);

	// setup clock to 16MHz internal Oscillator
	/*
	sc.clock_setup.oscillator = hal::sysctl::Oscillator::PrecisionInternal(
		hal::sysctl::SystemClock::UseOscillator(hal::sysctl::Divider::_1),
	);
	*/
	let clocks = sc.clock_setup.freeze();
	let mut delay = Delay::new(ti_core.SYST, &clocks);

	let mut porta = ti.GPIO_PORTA.split(&sc.power_control);
	let tx = porta.pa1.into_af_push_pull::<hal::gpio::AF1>(&mut porta.control);
	let rx = porta.pa0.into_af_push_pull::<hal::gpio::AF1>(&mut porta.control);

	let async_uart = AsyncUART0::new(
		ti.UART0,
		tx,
		rx,
		&mut sc.power_control,
		&clocks,
		115200.bps(),
		unsafe { &mut TX_BUF },
		unsafe { &mut RX_BUF },
	);

	cortex_m::interrupt::free(|cs| ASYNC_UART.borrow(cs).replace(Some(async_uart)));
	ti_core.NVIC.enable(Interrupt::UART0);

	let mut recv: [u8; 1] = [0; 1];
	loop {
		// echo the received bytes back
		cortex_m::interrupt::free(|cs| {
			if let Some(ref mut uart) = ASYNC_UART.borrow(cs).borrow_mut().deref_mut() {
				if uart.received_bytes() > 0 {
					uart.read(&mut recv);
					uart.write(&recv).unwrap();
				}
			}
		});

		delay.delay_ms(1u32);
	}
}

// register interrupt-handler for UART0
interrupt!(UART0, uart0_handler);

// interrupt-handler for UART0
fn uart0_handler() {
	cortex_m::interrupt::free(|cs| {
		if let Some(ref mut uart) = ASYNC_UART.borrow(cs).borrow_mut().deref_mut() {
			let isr_status = uart.uart.mis.read().bits();

			if (isr_status & (1 << 1)) > 0 {
				// CTSMIS
				// this bit is always set at startup for some reason -> clear it
				uart.uart.icr.write(|w| w.ctsmic().set_bit());
			} else if (isr_status & (1 << 4)) > 0 {
				// RXMIS
				// reading data-register clears interrupt
				let rec = (uart.uart.dr.read().bits() & 0xFF) as u8; // ignore error bits
				if !uart.rx_fifo.full() {
					uart.rx_fifo.push(rec).unwrap();
				}
			} else if (isr_status & (1 << 5)) > 0 {
				// TXMIS
				if uart.tx_fifo.empty() {
					uart.uart.icr.write(|w| w.txic().set_bit()); // clear interrupt
				} else {
					let snd = uart.tx_fifo.pop().unwrap() as u32;
					// writing to the data-register clears interrupt
					uart.uart.dr.write(|w| unsafe { w.bits(snd) });
				}
			} else {
				// some other interrupt
				// clear all interrupt-flags
				uart.uart.icr.write(|w| unsafe { w.bits(0xFFFF) });
			}
		}
	});
}

#[exception]
/// The hard fault handler
fn HardFault(ef: &cortex_m_rt::ExceptionFrame) -> ! {
	panic!("HardFault at {:#?}", ef);
}

#[exception]
/// The default exception handler
fn DefaultHandler(irqn: i16) {
	panic!("Unhandled exception (IRQn = {})", irqn);
}
