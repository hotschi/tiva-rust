#![no_std]	// no standard-library
#![no_main]	// no main-function

extern crate cortex_m;
extern crate panic_halt;
extern crate tm4c123x_hal as hal;

use core::cell::{Cell, RefCell};
use core::ops::DerefMut;
use cortex_m::interrupt::Mutex;
use cortex_m_rt::{entry, exception};
use hal::prelude::*;
use hal::tm4c123x::{interrupt, Interrupt, TIMER1};
use hal::{gpio, sysctl};

// Due to the ownership model and the borrow-checker in rust it's not possible
// to use global variables in the same way like in C. The reason is that every
// function is treated as a different context/thread so accessing global
// variables is unsafe. It can be done by either using an unsafe{} block, but
// this could cause real race-conditions (especially if interrupts are involved)
// or by using mutexes which make sure that only one context access a variable
// at a time. In this example the mutex-approach is applied.
static CNT: Mutex<Cell<u32>> = Mutex::new(Cell::new(0));
static LED: Mutex<RefCell<Option<gpio::gpiof::PF3<gpio::Output<gpio::PushPull>>>>> =
	Mutex::new(RefCell::new(None));
static TIM1: Mutex<RefCell<Option<hal::tm4c123x::TIMER1>>> = Mutex::new(RefCell::new(None));

fn enable_peripheral(p: sysctl::Domain, sc: &mut hal::sysctl::PowerControl) {
	// This function is used to enable the desired peripheral.
	// If this is not done either the registers are not mapped into memory or
	// they just don't respond because they are not powered on. In each case
	// you will get a hardfault because the desired address is not available.
	sysctl::control_power(sc, p, sysctl::RunMode::Run, sysctl::PowerState::On);
	sysctl::control_power(sc, p, sysctl::RunMode::Sleep, sysctl::PowerState::On);
	sysctl::reset(sc, p);
}

fn config_timer(tim: &mut TIMER1) {
	tim.ctl.write(|w| w.taen().clear_bit()); // disable timer
	tim.cfg.write(|w| unsafe { w.bits(0x00) }); // configure to 32bit mode

	// configure timer to count down periodically and enable interrupt at timeout
	tim.tamr.write(|w| unsafe { w.tamr().bits(2).tacdir().clear_bit() });
	tim.tailr.write(|w| unsafe { w.bits(2_000_000) }); // raise interrupt every ~125ms at 16MHz
	tim.imr.write(|w| w.tatoim().set_bit()); // enable timeout interrupt of TimerA
	tim.ctl.write(|w| w.taen().set_bit()); // enable timer again
}

#[entry]
fn main() -> ! {
	let mut ti = hal::Peripherals::take().unwrap(); // take the TIVA peripherals
	let mut ti_core = hal::CorePeripherals::take().unwrap(); // take the core peripherals

	let mut sc = ti.SYSCTL.constrain();

	// If PLL is used with the main-clock, openocd creates errors for some reason..
	/*
	sc.clock_setup.oscillator = hal::sysctl::Oscillator::Main(
		hal::sysctl::CrystalFrequency::_16mhz,
		hal::sysctl::SystemClock::UsePll(hal::sysctl::PllOutputFrequency::_80_00mhz),
	);
	*/

	// setup clock to 16MHz internal Oscillator
	sc.clock_setup.oscillator = hal::sysctl::Oscillator::PrecisionInternal(
		hal::sysctl::SystemClock::UseOscillator(hal::sysctl::Divider::_1),
	);

	// config clocks once and then freeze it -> they cannot be changed anymore (from software)
	let _clocks = sc.clock_setup.freeze();

	// configure the timer
	enable_peripheral(sysctl::Domain::Timer1, &mut sc.power_control); // enable timer
	config_timer(&mut ti.TIMER1); // config timer appropriatly

	// configure the necessary mutexes in order to access the values safely in the interrupt
	let t1 = ti.TIMER1;
	cortex_m::interrupt::free(|cs| TIM1.borrow(cs).replace(Some(t1)));
	let portf = ti.GPIO_PORTF.split(&sc.power_control);
	let led_green = portf.pf3.into_push_pull_output();
	cortex_m::interrupt::free(|cs| LED.borrow(cs).replace(Some(led_green)));

	// AFTER creating the mutexes enable the interrupts
	ti_core.NVIC.enable(Interrupt::TIMER1A);

	loop {}
}

// register an own interrupt-handler for all interrupts of TIMER1A
interrupt!(TIMER1A, timer1a_handler);

// interrupt-handler for TIMER1A
fn timer1a_handler() {
	static mut LED_STATE: bool = false;

	// increment CNT variable
	cortex_m::interrupt::free(|cs| {
		// interrupts are disabled in here -> access CNT safely
		let cnt = CNT.borrow(cs).get();
		CNT.borrow(cs).set(cnt + 1);
	});

	// toggle LED
	cortex_m::interrupt::free(|cs| {
		// interrupts are disabled in here -> access LED safely
		if let Some(ref mut led) = LED.borrow(cs).borrow_mut().deref_mut() {
			// this hack is done because there exists no interface to read the led-status
			let state = unsafe {
				LED_STATE = !LED_STATE;
				!LED_STATE
			};

			if state {
				led.set_low();
			} else {
				led.set_high();
			}
		}
	});

	// clear intterupt-flag (by accessin TIM1)
	cortex_m::interrupt::free(|cs| {
		// interrupts are disabled in here -> access TIM1 safely
		if let Some(ref mut timer) = TIM1.borrow(cs).borrow_mut().deref_mut() {
			timer.icr.write(|w| w.tatocint().set_bit());
		}
	});
}

#[exception]
// The hard fault handler
fn HardFault(ef: &cortex_m_rt::ExceptionFrame) -> ! {
	panic!("HardFault at {:#?}", ef);
}

#[exception]
// The default exception handler 
// is executed when e.g. an interrupt is raised where no handler was registered
fn DefaultHandler(irqn: i16) {
	panic!("Unhandled exception (IRQn = {})", irqn);
}
