# To-Do List

## Projects/Programming

### Examples

- [x] Add SPI-example
- [x] Add I2C-example
- [x] Add UART-example
- [x] Add asynchronous Timer-example
- [x] Add asynchronous UART-example

### General Problems

- [x] Configure the debugger in a way to view the internal register-values
- [x] Find a way to provide own ISR-routines
- [ ] Find a way to modify tm4c-hal in order to implement own features

## Documentation

- [x] Writing tutorial for setting up toolchain/environment
- [x] Provide description of the project-structure and the reason