# Problems, Issues and Bugs

+ **Clock Configuration:**  
	If the clock of the Tiva controller is configured to 80MHz PLL for some
	reason openOCD stops working. It always throws some errors with 'reading' or
	'writing' memory failed. If the clock is configured in a way where the
	system-clock is not configured to PLL and not derived from the main clock,
	openOCD works perfectly. Unfortunately I don't know if this is a bug in
	openOCD itself or in the debugger on the launchpad itself.

+ **UART problems with wrong clock:**  
	If the system-clock is not derived from the main-clock and set to 80MHz via
	PLL, the UART is not working properly. The timing is somehow broken and only
	wrong characters are transmitted and received. Unfortunately the required
	clock-configuration is exactly the opposite of the required one for getting
	openOCD working properly. Unfortunately I also don't know the reason for
	this problem, but it could be that the prescaler-registers have to be
	configured more precisely or in a different way than now.