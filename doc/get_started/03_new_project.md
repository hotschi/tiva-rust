# Creating a new Project

If you have already successfully complied and debugged an application, creating
a new project is rather easy. Just take care that you don't miss any of the
following steps:

1. In the 'projects'-directory there is a folder called 'skeleton'. This is the
	'skeleton' for a project. It contains the absolute minimum of files and
	source-code, it just compiles and does nothing. Make a copy of this 
	directory and give it the name of your project.

2. Open the file 'Cargo.toml' of your new project and change the following 
	properties in this file so that they fit to your project:

	+ **package**  
		- authors
		- name
	+ **bin**  
		- name

3. Go back to the root of the workspace and open the file 'Cargo.toml'. Then add
	the path to your new project to the member-list. This list keeps track of
	all projects in this workspace, and if you  don't mention your project in 
	this list, you won't be able to compile the project.

4. Open the file .vscode/launch.json. This files contains all the
	debug-configurations for every project. Just copy one configuration and
	paste it at the bottom of the list as a new entry/config. Then change the
	following two properties:

	+ **name**  
		You can type in here whatever you want, I just always took the name of
		the project and appended 'GDB Debug', but that's completely up to you.
		This is just the description you find when you choose which project you
		want to debug (in the debug-section in VSCode).

	+ **target**  
		The target points to the actual compiled binary which will be loaded to 
		the hardware. You don't have to modify the whole path here. Due to the
		setup of the build-system, the binaries of all projects are located in
		the same directory. So you only have to adjust the file-name with the
		one you provided in the 'Cargo.toml' of your project.

5. In your new project there is a file called 'README.md'. This file is used to 
	provide a brief description of what your project does. But providing such a
	description is of course optional, but I think it' always a good idea to
	just explain the functionality in a few words, so that other people are also
	able to understand what this software is doing.

