# How to build, flash and debug an application on the TIVA

This tutorial describes how to build an application, flash it and debug it
directly on the hardware.

The general approach for here is to use the cargo build-system to generate the
binaries and openOCD to to establish a debug connection to the TIVA board. If 
openOCD is running, one can use gdb and connect via a remote-connection 
(IP 127.0.0.1 port 3333) to openocd and execute nearly all gdb-commands which 
are available.

## Building

In order to build the projects and generate binaries for the TIVA, the cargo
build-system is used. There basically exist three commands which are currently
relevant for this project:  

+ ``cargo fmt``  
	This formats all the source-code according to the standard rust 
	coding-style. In the root of this repository is a file called 'rustfmt.toml'
	which can be used to adapt the coding-style to own wishes. Currently it is
	set to a maximum line-width of 100 characters and using tabulators instead
	of spaces. Of course it's possible to set a lot more options, the following
	link shows all available options: 
	(https://github.com/rust-lang/rustfmt/blob/master/Configurations.md)

+ ``cargo check``  
	This command basically just checks the syntax of your source-code and your
	setup of the build-system and tells you if the project is ready to compile  

+ ``cargo build``  
	This command does the same like ``cargo check`` but also compiles all the 
	source code and creates the desired binary  

+ ``cargo run``  
	This command checks and build your source code like the two above but also 
	runs the binary. In the case of this project it makes no sense to actually 
	run the program because it is compiled for the TIVA. But it possible to 
	overwrite this command with a desired one. This was done and now the
	compiled binary gets loaded into the flash of the TIVA with gdb via an
	opened openOCD connection.

The recommended way for building a project is, to do this by command line. In
VSCode there exists an integrated terminal which is I think very useful. You can
activate it by 'Terminal->New Terminal'.  
After you openend a terminal change into the directory of your desired project
(eg. ``cd projects/quick-start``). The run cargo build and your project will be
compiled. 

If you run ``cargo build`` somewhere else than in a project-directory it won't 
the according 'Cargo.toml' file (which is like a makefile for C/C++ projects) 
and it will give you an error. 

However it is possible to compile the project from the root-directory of this 
workspace. You can run ``cargo build -p <project-name>`` e.g. 
``cargo build -p uart-test`` but I won't recommend this way of building because 
``cargo run`` won't work in the root-directory.

All the compiled binaries are stored in the directory 'target' which is located
in the root of the workspace. The reason for this is, that all the common 
packages which are needed by the different projects only have to be compiled 
once. This saves a lot of time and disk-space. So all binaries of all projects
can be found in 'target/thumbv7em-none-eabihf'.

## Flashing a binary to the TIVA

1. In order to flash a program it is necessary to start openOCD. I recommend to 
	do this in a separate terminal because this should run in the background as 
	long as	the TIVA is connected to the PC in order to provide access to it. 

	So open a separate terminal, change to the workspace-root an execute:
	```console
	$ openocd
	```
	openOCD uses the 'openocd.cfg' in the workspace and connects to the target.

2. After establishing the openOCD connection, change with a separate terminal
	in the desired project folder (I recommend using one of the integrated
	terminals within VSCode) and just execute ``cargo run``. This command
	compiles the project, starts a gdb-session, loads the binary into the flash
	of the device and quits the gdb-session again. 

	Unfortunately TI uses some proprietary protocol to communicate with the 
	on-chip-debugger on the TIVA and this protocol is not perfectly supported by
	openOCD. This means that flashing can result in an error-message although
	the flashing-procedure worked as expected: Below you find an example of one
	error-message:

	```console
	Loading section .vector_table, size 0x400 lma 0x0
	Loading section .text, size 0x6686 lma 0x400
	Loading section .rodata, size 0x8f4 lma 0x6a90
	Start address 0x691c, load size 29562
	Transfer rate: 14 KB/sec, 7390 bytes/write.
	adapter speed: 21933 kHz
	memory write failed: 0x7
	memory write failed: 0x7
	memory write failed: 0x7
	in procedure 'reset' 
	in procedure 'ocd_bouncer'


	memory read failed: 0x7
	jtag status contains invalid mode value - communication failure
	Polling target tm4c123gh6pm.cpu failed, trying to reexamine
	memory read failed: 0x7
	Examination failed, GDB will be halted. Polling again in 100ms
	[Inferior 1 (Remote target) detached]
	```

	The error-message itself is mostly not that important, you just have to make
	sure that the following lines are printed:

	```console
	Loading section .vector_table, size 0x400 lma 0x0
	Loading section .text, size 0x6686 lma 0x400
	Loading section .rodata, size 0x8f4 lma 0x6a90
	Start address 0x691c, load size 29562
	Transfer rate: 14 KB/sec, 7390 bytes/write.
	```

	If these lines are printed, then the flash has been written successfully.
	The error-messages can occur if the clock of the TIVA is configured in a way
	that the main-clock is used and/or the PLL is activated. The reason why this
	causes openOCD to crash (and also entering the hardfault-hander) is
	currently unknown. If you just want to flash the program, these errors
	shouldn't be a problem, you just have to reset the TIVA and the program
	should start running.  
	However, if you want to debug the application, make sure to use another 
	clock-configuration to avoid those errors, otherwise the debugger won't
	start and will throw you a lot of errors. 

## Debugging

If you followed the tutorial with installing the toolchain correctly, this part
should work more or less out of the box. The whole debugging-process should be
integrated into VSCode, done by gdb and executed by openOCD. Lets begin:

1. If you have a running session of openOCD close it, otherwise the debugger
	won't be able to start.

2. On the vertical panel on the left side of VSCode you can see an image of a
	bug with an 'X' on its back. Click there.

3. Then on the left panel on the top there it says 'Debug' and you can choose
	a debug-configuration from the drop-down menu. The structure of this project
	creates for every project an down debug-configuration. Choose one config and
	hit the green arrow to the left. If openOCD is running and you did 
	everything correct, the debug-session should start and VSCode should jump
	to the main-file of you chosen project and break at the beginning of the
	main-function.

4. The actual debugging works very similar to all other IDEs like Eclipse, 
	QT-Creator, etc. You can add breakpoints, watch variables, etc.

### Ubuntu and other Distros

If you are on Ubuntu (or some other distro, where the name of the debugger is
not 'arm-none-eabi-gdb') this probably won't work out of the box because the 
package-name for the debugger differs from ArchLinux. You have now 3 options to 
to get the debugging working in other distros:

1. Open the file and change all properties ``"gdbpath": "arm-none-eabi-gdb"`` to
	``"gdbpath": "gdb-multiarch"``. However this can be very cumbersome and also
	you would ruin your git-history with these changes so I would recommend one 
	of the next two options

2. Create a symbolic link from 'gdb-multiarch' to 'arm-none-eabi-gdb':
	1. Find out the installation-path of 'gdb-multiarch':
		```console
		$ which gdb-multiarch
		```
	2. Create a symbolic link:
		```console
		$ sudo ln -s <path-to-gdb-multiarch> /usr/bin/arm-none-eabi-gdb
		```
	If somebody doesn't want to mess up his or her binary-directories there is 
	still the third method.

3. Create an alias for this name:
	1. Open the file '/etc/bash.bashrc':
	```console
	$ sudo nano /etc/bash.bashrc
	```

	2. Append to the end of the file the following:
	```console
	alias arm-none-eabi-gdb='gdb-multiarch'
	```

	After saving the file a reboot is required. I don't think that logging out
	and in again will be enough to reload this file. 

For sure there are more than these three methods, but I think these are the 
easiest and fastest ones to do.