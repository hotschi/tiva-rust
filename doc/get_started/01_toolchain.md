# Toolchain Setup for Flashing and Debugging the TIVA TM4C123G Launchpad

This is a tutorial on how to setup the toolchain in order to compile and debug
applications on the TIVA Launchpad with Rust and GDB. It focuses only on this
specific controller/eval-board. If others are wanted it may be necessary to 
intall other packages (e.g. the compiler-backend for ARM Cortex M0+). 

This tutorial is written only for ArchLinux, Manjaro and all distributions based 
on Ubuntu 18.04 (Ubuntu, Linux Mint, Elementary OS, etc.). If you have any other
distribution or even another operating-system, the packages to install may have
different names, but all commands which are not installing something should be
the same. However if you still need help, just visit the following website:
https://docs.rust-embedded.org/book/intro/install.html

## Installing Compiler, Debugger and OpenOCD

1. Installing the rust-compiler. The rust community recommends using the tool
	'rustup' in order to install the compiler. The following command will
	download a script which downloads and installs rustup with the complete
	rust-toolchain:  

	```console
	$ curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
	```

2. After the rust-compiler as well as the buildystem 'cargo' is installed, the
	compiler-backend for the TIVA (ARM Cortex M4F) has to be installed:  

	```console
	$ rustup target add thumbv7em-none-eabihf
	```

3. After installing the compiler, we proceed by installing the GDB-debugger as
	well as 'OpenOCD' which is an interface between the debugging-port of the
	microcontroller and the GDB.  

	**ArchLinux:**

	```console
	# pacman -S arm-none-eabi-gdb openocd jq
	```

	**Ubuntu:**

	```
	$ sudo apt install gdb-multiarch openocd jq
	```

4. OpenOCD needs access to the USB-port where the TIVA is connected. In Linux
	this is in general not allowed without root-privileges. However creating the 
	following udev-rule will enable running OpenOCD without root privileges.  
	
	```console
	$ sudo nano /etc/udev/rules.d/70-tiva.rules
	```

	After creating this new file, enter the following:  

	```console
	# TI TIVA Launchpad TM4C123G
	ATTRS{idVendor}=="1cbe", ATTRS{idProduct}=="00fd", TAG+="uaccess"
	```

	When the file was created successfully, execute the following command in
	order to update the udev-rules:
	
	```console
	$ sudo udevadm control --reload-rules
	```
5. In order to test the connection with OpenOCD to the actual hardware, open a
	terminal and change to the root-directory of this project. There is a file
	called 'openocd.cfg' located. Then connect the TIVA to the PC via USB and
	try to establish an OpenOCD-connection:  
	
	```console
	$ openocd
	```

	If the connection was successful, you should an output similar to the one
	below:

	```console
	Open On-Chip Debugger 0.10.0
	Licensed under GNU GPL v2
	For bug reports, read
			http://openocd.org/doc/doxygen/bugs.html
	Info : The selected transport took over low-level target control. 
	The results might differ compared to plain JTAG/SWD
	
	adapter speed: 500 kHz
	Info : RCLK (adaptive clock speed)
	Info : ICDI Firmware version: 9270
	Info : tm4c123gh6pm.cpu: hardware has 6 breakpoints, 4 watchpoints
	```

## Installing IDE

Now that the basic toolchain is installed, it is probably very useful to also
install some kind of IDE. In this tutorial I will only describe how to install 
the IDE 'Visual Studio Code'. I think it is currently the best IDE when you are
developing applications in Rust (of course there are still Vim and emacs, but I
really don't know how to use those properly). Again, this tutorial is just for
ArchLinux, Manjaro and all distributions based on Ubuntu 18.04.

1. The first part is to install Visual Studio Code: 

	**Archlinux:**

	```console
	# pacman -S code
	```

	**Ubuntu:**    

	```console
	$ sudo apt update
	$ sudo apt install software-properties-common apt-transport-https wget

	$ wget -q https://packages.microsoft.com/keys/microsoft.asc -O- | sudo apt-key add -

	$ sudo add-apt-repository "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main"

	$ sudo apt update
	$ sudo apt install code
	```

2. After the IDE was installed, it is necessary to install two extension/addons
	within Visual Studio Code. This can be done by starting the IDE, and 
	clicking on the left vertical panel on the symbol with the 4 squares. 
	+ **Rust (rls)**  
		An addon for syntax-highlighting and code-completion for the Rust 
		programming language

	+ **Cortex-Debug**
		An addon which makes it possible to debug ARM Cortex M CPUs. With the
		help of the 'tools/tm4c123gh6pm.svd' it is possible to view and edit all
		the CPU-register of the ARM core as well as the peripheral registers of
		this TI controller.

	+ **Better Toml (optional)**  
		An addon which adds support for syntax-highlighting in *.toml files.
		This can be very useful if you want to do some changes/adjustments in
		the cargo build-configurations.

	+ **Native Debug (optional)**  
		An addon which enables the integration of GDB and LLDB within Visual
		Studio Code. It is just a general addon for GDB or LLDB, with this one
		it is not possible to view the registers of the TIVA for example. The
		addon is not necessary for this project, but maybe you want to debug
		some code which is running on your OS and not on the TIVA.

	+ **C/C++ (optional)**  
		Enables syntax-highlighting and code-completion for C and C++. This
		addon is not necessary but it's recommendable to install it because it
		happens that at some time you have to import C/C++ libraries/files into 
		Rust-Projects.

3. Now the setup is completed, it's time to launch the IDE. Start the IDE and go
	to 'File->Open Folder' and choose the root directory of this project. VS-Code
	should load the provided configurations from the directory .vscode/ and you
	should be able to compile, flash and debug all the application within this
	project. How this can be done is described in the file 
	'/doc/get_started/02_building.md'.

4. If the IDE is started the first time, and a project is opened, the extension
	RLS (rust language server) compiles all the available projects in order to
	provide the code-completion for the source-files. This can take quite a time
	(up to a few minutes depending on your hardware) because the rust-compiler
	is not that fast (as for e.g. a C/C++ compiler) and has to compile all the 
	necessary modules. But this is done only once and after this it should
	become way faster due to the incremental compilation (Unless you execute
	``cargo clean``). The compiled binaries for RLS are stored in 'target/rls'.