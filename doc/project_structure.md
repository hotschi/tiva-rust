# File-structure of this project

## Setup of the repository

The root of this repository consists of 6 major folders:

1. **.vscode:**  
	This folder contains all the config-files for VSCode concerning this
	project: The debug-config 'launch.json' which contains a configuration for 
	all the projects in order to use the 'Cortex-Debug' addon and the config for
	the IDE	itself (font-size, line-width, etc.).

2. **doc:**  
	Contains all the documentation files for this project as well as all the
	relevant datasheet which are needed for the different projects.

3. **crates**  
	This directory is currently empty, but it is supposed to contain self-written
	crates which can then be used within multiple projects.

4. **projects:**  
	This directory contains all final projects which can be compiled into
	executable binaries, or in other words every of these projects contains a
	main-function.

5. **target:**  
	This directory contains all sort of compiled binaries. It contains the final
	compiled binaries of the projects, all the intermediate files which are
	created by the compiler and all binaries created by the rust language-server
	(rls).

6. **tools:**  
	A directory which contains files which don't belong to somewhere else.

## Reasons for this setup

The main-reason why this repository is structured like this is due to the way
the buils-system 'cargo' works:

In the root of the repo is a file called 'Cargo.toml'. This files is the 
config-file for a so called 'Workspace' for cargo. This file mentions all the 
available projects within this repo and the general build-configuration.

Every project itself contains also a 'Cargo.toml' which contains things like
project-name, author, etc. and the build-dependencies.

Cargo as a build-system realizes that this repository is structured as a
workspace and compiles all the projects and libraries in the same directory
namely 'target'. This is the big advantage of this structure: Due to the fact
that all binaries are compiled to the same location it is not necessary to 
compile common libraries, crates or files for every project on its own but use 
the compiled files multiple times for linking them into different binaries.

If this would be a C/C++ project or even a go-project, this setup still wouldn't
make sense because these compilers are pretty fast (especially the go-compiler).
However the rust-compiler is compared to the others very slow (building this 
workspace from scratch could take a few minutes depending on your hardware) and
produces also pretty big binaries (the target folder has with all compiled
projects around 600MB). Now due to these drawbacks of the compiler it makes
totally sense to compile every needed thing just once and use it, if necessary,
multiple times. This really speeds up the compile-times and also your PC itself
because the rust language-server also has to compile the sources just once.
