# This file gets loaded if 'cargo run' is executed
# It connects gdb to a running openocd-instance, flashes the binary and quits
# gdb again

target remote :3333
set confirm on
load
monitor exit
monitor reset
detach
quit