 # Rust on the TIVA TM4C123G Launchpad

This is a project which tries to use the Rust programming-language in order
to program applications on the TIVA launchpad. This project aims to provide a
kind of workspace where different projects can be created, builded and debugged
with the same IDE and without changing any configuration. 

All the projects are just focused on the TIVA, and they make use of basically
2 crates:  

+ **cortex-m**  
	A crate, which contains all rust-wrapped register-description for the 
	ARM processor. This crate supports AFAIK Cortex M0/M1/M0+, Cortex M3,
	Cortex M4/M4F and Cortex M7/M7F.  

+ **tm4c123x-hal**  
	A crate which contains all the register-descriptions for the TM4C123G
	processor as well as the implemented functions according to the 
	'embedded-hal' crate.

In 'doc/get_started/' you find tutorials/manuals how to get the toolchain
working, how to compile, flash and debug applications and how to create your own
projects.